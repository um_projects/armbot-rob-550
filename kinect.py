import cv2
import numpy as np
from PyQt4.QtGui import QImage
import freenect
import Depth_detectblock
from apriltag import apriltag

class Kinect():
    def __init__(self):
        self.currentVideoFrame = np.array([])
        self.currentDepthFrame = np.array([])
        
        if(freenect.sync_get_depth() == None):
            self.kinectConnected = False
        else:
            self.kinectConnected = True
        
        # mouse clicks & calibration variables
        #self.depth2rgb_affine = np.float32([[1,0,0],[0,1,0]])
        #self.depth2rgb_affine = np.float32([[1.00294423e+00,7.51337476e-05,-1.39662201e-01],[9.00474911e-05,1.00011838e+00,3.36899561e-01]])
        self.depth2rgb_affine = np.float32([[  9.33032351e-01, -3.28560156e-03, 6.92178173e+00],
                                            [  2.89633874e-03,  9.28260592e-01, 3.50516317e+01]])
        self.kinectCalibrated = True
        self.robotCalibrated = False
        self.last_click = np.array([0,0])
        self.new_click = False
        self.rgb_click_points = np.zeros((5,2),int)
        self.depth_click_points = np.zeros((5,2),int)

        """ Extra arrays for colormaping the depth image"""
        self.DepthHSV = np.zeros((480,640,3)).astype(np.uint8)
        self.DepthCM=np.array([])

        """ block info """
        self.block_contours = np.array([])
        self.camera_matrix=[]
        self.distort_coeffs=[]
        self.loadCameraCalibration()
        self.image2WorldMatrix=[]
        self.loadExtrinsicCalibration()


    def captureVideoFrame(self):
        """                      
        Capture frame from Kinect, format is 24bit RGB    
        """
        if(self.kinectConnected):
            self.currentVideoFrame = self.intrinsicCorrect(freenect.sync_get_video()[0])
        else:
            self.loadVideoFrame()
        self.processVideoFrame()

    def processVideoFrame(self):
        cv2.drawContours(self.currentVideoFrame,self.block_contours,-1,(255,0,255),3)


    def captureDepthFrame(self):
        """                      
        Capture depth frame from Kinect, format is 16bit Grey, 10bit resolution.
        """
        if(self.kinectConnected):
            if(self.kinectCalibrated):
                self.currentDepthFrame = self.registerDepthFrame(freenect.sync_get_depth()[0])
            else:
                self.currentDepthFrame = freenect.sync_get_depth()[0]
        else:
            self.loadDepthFrame()

    
    def loadVideoFrame(self):
        self.currentVideoFrame = cv2.cvtColor(
            cv2.imread("data/ex0_bgr.png",cv2.IMREAD_UNCHANGED),
            cv2.COLOR_BGR2RGB
            )

    def loadDepthFrame(self):
        self.currentDepthFrame = cv2.imread("data/ex0_depth16.png",0)

    def convertFrame(self):
        """ Converts frame to format suitable for Qt  """
        try:
            img = QImage(self.currentVideoFrame,
                             self.currentVideoFrame.shape[1],
                             self.currentVideoFrame.shape[0],
                             QImage.Format_RGB888
                             )
            return img
        except:
            return None

    def convertDepthFrame(self):
        """ Converts frame to a colormaped format suitable for Qt  
            Note: this cycles the spectrum over the lowest 8 bits
        """
        try:

            """ 
            Convert Depth frame to rudimentary colormap
            """
            self.DepthHSV[...,0] = self.currentDepthFrame
            self.DepthHSV[...,1] = 0x9F
            self.DepthHSV[...,2] = 0xFF
            self.DepthCM = cv2.cvtColor(self.DepthHSV,cv2.COLOR_HSV2RGB)
            cv2.drawContours(self.DepthCM,self.block_contours,-1,(0,0,0),3)

            img = QImage(self.DepthCM,
                             self.DepthCM.shape[1],
                             self.DepthCM.shape[0],
                             QImage.Format_RGB888
                             )
            return img
        except:
            return None

    def getAffineTransform(self, coord1, coord2):
        """
        Given 2 sets of corresponding coordinates, 
        find the affine matrix transform between them.

        TODO: Rewrite this function to take in an arbitrary number of coordinates and 
        find the transform without using cv2 functions
        """
        # pts1 = coord1[0:3].astype(np.float32)
        # pts2 = coord2[0:3].astype(np.float32)
        # print(cv2.getAffineTransform(pts1,pts2))
        # return cv2.getAffineTransform(pts1,pts2)
        coord1 = np.array(coord1)
        coord2 = np.array(coord2)

        #if (np.array(coord1)).shape[1] == 2:
        
        coord1 = np.c_[coord1,np.ones((np.array(coord1)).shape[0])]

        coord1 = coord1.transpose()   
        coord2 = coord2.transpose()
        matrix= np.matmul(coord2,np.linalg.pinv(coord1))
    
        return matrix


    def registerDepthFrame(self, frame):
        """
        TODO:
        Using an Affine transformation, transform the depth frame to match the RGB frame
        """
        return cv2.warpAffine(frame, self.depth2rgb_affine, (640,480))
        #return frame

    def loadCameraCalibration(self):
        f=open('calibration.cfg','r')
        f.readline()
        
        #try: 
        for i in range(3):
            line=f.readline().replace(']','').replace('[','')
            mat_row=[]
            for num in line.split():
                mat_row.append(float(num))
            self.camera_matrix.append(mat_row)

        print(self.camera_matrix)
        self.camera_matrix=np.array(self.camera_matrix)
        f.readline()
        
        line= f.readline().replace(']','').replace('[','')
        for num in line.split():
            self.distort_coeffs.append(float(num))
        #distort.append()
        
        line= f.readline().replace(']','').replace('[','')
        self.distort_coeffs.append(float(line))
        print(self.distort_coeffs)
        self.distort_coeffs=np.array(self.distort_coeffs)

        f.readline()
        '''
        for i in range(2):
            line=f.readline().replace(']','').replace('[','')
            mat_row=[]
            for num in line.split():
                mat_row.append(float(num))
            self.depth2rgb_affine.append(mat_row)
        
        '''
        f.close()

        #except ValueError:
            #print("Exception encountered")
    def loadExtrinsicCalibration(self):
        f=open('last_extrinsic_cal.cfg','r')
        f.readline()
        
        #try: 
        for i in range(3):
            line=f.readline().replace(']','').replace('[','')
            mat_row=[]
            for num in line.split():
                mat_row.append(float(num))
            self.image2WorldMatrix.append(mat_row)

        print(self.image2WorldMatrix)
        self.image2WorldMatrix=np.array(self.image2WorldMatrix)
        self.robotCalibrated=True
        '''
        for i in range(2):
            line=f.readline().replace(']','').replace('[','')
            mat_row=[]
            for num in line.split():
                mat_row.append(float(num))
            self.depth2rgb_affine.append(mat_row)
        
        '''
        f.close()

    def writeExtrinsicMatrix(self):
        f = open('last_extrinsic_cal.cfg', 'w')
        f.write("Extrinsic matrix:\r\n")
        f.write(str(self.image2WorldMatrix))
        f.close() 

    def blockDetector(self):
        """
        TODO:
        Implement your block detector here.  
        You will need to locate
        blocks in 3D space
        """
        return Depth_detectblock.detectall(self.currentDepthFrame,self.currentVideoFrame, self)

    def detectBlocksInDepthImage(self):
        """
        TODO:
        Implement a blob detector to find blocks
        in the depth image
        """
        pass

    # Unused
    '''
    def worldTransformMatrix(self, model_points, image_points, camera_matrix, dist_coeffs):

        [success, rot, trans] = cv2.solvePnP(model_points, image_points, camera_matrix, dist_coeffs, flags=cv2.SOLVEPNP_ITERATIVE)
        if success:
            aug1=np.column_stack(rot,trans)
            aug2=np.concatenate(aug1,[0,0,1,0])
            print(aug2)
        else:
            print("Error")
        return aug2
    '''
    def intrinsicCorrect(self,curr_Frame):
        h=480
        w=640
        #[camera_matrix, dist_coefs]=self.loadCameraCalibration()
        new_camera_matrix, roi = cv2.getOptimalNewCameraMatrix(self.camera_matrix,self.distort_coeffs,(w,h),1,(w,h))
        #rgb_frame = cv2.cvtColor(curr_Frame,cv2.COLOR_RGB2BGR)
        undistorted = cv2.undistort(curr_Frame, self.camera_matrix, self.distort_coeffs, None, new_camera_matrix)
        
        return undistorted
    '''
    def depthTransform(self,d):
        # in mm
        real = 123.6 * np.tan((d/2842.5) + 1.1863)

        return real

    def depthRGBCalibrate(self, image_points, depth_points):
        self.depth2rgb_affine = self.getAffineTransform(depth_points, image_points)
        self.kinectCalibrated=True
        print(self.depth2rgb_affine)
    '''
    def calibrate(self, image_points, actual_world_xy):

        self.image2WorldMatrix = self.getAffineTransform(image_points, actual_world_xy)
        print("Image2World Matrix",self.image2WorldMatrix)

        self.robotCalibrated=True

    def aprilTagDetect(self):
        
        detector = apriltag("tagStandard41h12")
        imagePoints=[]
        totalDetections=[]
        added=False
        iterations=0
        
        #repeat multiple times to ensure all points are obtained.
        while len(totalDetections)<7:
            frame = self.currentVideoFrame
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            detections = detector.detect(frame)
            depthFrame=self.currentDepthFrame
            for j in range(len(detections)):
                added=False
                #print(detections[0]['lb-rb-rt-lt'])
                for k in range(len(totalDetections)):
                    if detections[j]['id']==totalDetections[k]['id']:
                        added=True
                if not added:
                    totalDetections.append(detections[j])
                    print("detected ",detections[j]['id'])
            iterations+=1
            #print(iterations)
            
            if iterations>1000:
                print("Cannot detect all aprilTags")
                print(totalDetections)
                break
        
        if len(totalDetections) is 7:
            worldPositions=[    [   0,   0,139],#Center -2 
                                [-270,-268,  0],#LB -3
                                [-270, 258,  0],#LT -4
                                [ 259, 258,  0],#RT -5
                                [ 259,-268,  0],#RB -6
                                [  20,-268,  0],#MB -7
                                [  20, 258,  0],#MT -8
                                                ] 
            for i in range(7):
                for detection in totalDetections:
                    [x,y]=detection['center']
                    print(x,y)
                    if detection['id'] is i+2:
                        imagePoints.append([x,y,depthFrame[int(y)][int(x)]])
        return imagePoints, worldPositions

    def transformImage2World(self,x,y,z):

        mouseXY=np.array([x,y,z,1]).transpose()
        #print(type(mouseXY))
        #print("MouseXY "+ str(mouseXY))
        #print("Matrix " + str(self.image2WorldMatrix))
        worldXY=np.matmul(self.image2WorldMatrix,mouseXY)

        #worldZ=self.depthTransform(z)
        
        return worldXY[0], worldXY[1], worldXY[2]

    
    
        