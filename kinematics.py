import numpy as np
#expm is a matrix exponential function
from scipy.linalg import expm
import sys

""" 
TODO: Here is where you will write all of your kinematics functions 
There are some functions to start with, you may need to implement a few more

"""
# Angle transformations
D2R = 3.141592/180.0
R2D = 180.0/3.141592
CLOSETOZERO = 1e-3

# DH Table formation
## Setting arm parameters
alpha = np.array([0, -90, 0, 90, -90, 90])*D2R
a = np.array([0, 0, 101.3, 0, 0, 0])
d = np.array([0, 0, 0, 109.90, 0, 0])
theta_offset = np.array([0, -90, 90, 0, 0, 0])*D2R

## Putting into the DH table
dh = np.zeros([6, 4])
dh[:, 0] = alpha
dh[:, 1] = a
dh[:, 2] = d

d_gripper = 140
T_w0 = np.array([1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 117.14, 0, 0, 0, 1]).reshape((4, 4))
T_6g = np.array([1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, d_gripper, 0, 0, 0, 1]).reshape((4,4)) 
#T_6g = np.identity(4)

class FK_Error(Exception):
    def __init__(self):
        print ("FK_Error: please check the link dimension in FK_dh()")
        sys.exit()

class Inv_Error(Exception):
    def __init__(self):
        print ("Inv_Error: input T is not type 'np.ndarray'")
        sys.exit()

class IK_Error(Exception):
    def __init__(self):
        print ("IK_Error: pose not reachable!")

def FK_dh(joint_angles, link = 5, grpTF = False):
    """
    TODO: implement this function

    Calculate forward kinematics for rexarm using DH convention

    return a transformation matrix representing the pose of the 
    desired link

    note: phi is the euler angle about the y-axis in the base frame

    """
    ## Initialization
    out = np.identity(4)
    theta = np.array(joint_angles) + theta_offset
    dh[:, 3] = theta

    # Function of calculating tfmat for one link
    def dh2tf_1link(link):
        ## Forming transformation matrix
        ## For columns: alpha = 0; a = 1; d = 2; theta = 3
        cos = np.cos
        sin = np.sin
        out = np.identity(4)
        out[0, 0] = cos(dh[link, 3])
        out[0, 1] = -sin(dh[link, 3])
        out[0, 3] = dh[link, 1]
        out[1, 0] = sin(dh[link, 3])*cos(dh[link, 0])
        out[1, 1] = cos(dh[link, 3])*cos(dh[link, 0])
        out[1, 2] = -sin(dh[link, 0])
        out[1, 3] = -dh[link, 2]*sin(dh[link, 0])
        out[2, 0] = sin(dh[link, 3])*sin(dh[link, 0])
        out[2, 1] = cos(dh[link, 3])*sin(dh[link, 0])
        out[2, 2] = cos(dh[link, 0])
        out[2, 3] = dh[link, 2]*cos(dh[link, 0])
        
        return out

    ## Specify if the links input is a int or list
    if type(link) is int:
        if link < 0 or link > 5:
            raise FK_Error
        for i in range(0, link + 1):
            out = out.dot(dh2tf_1link(i))

    if type(link) is list:
        if len(link) != 2:
            raise FK_Error
        if link[0] > link[1]:
            raise FK_Error
        for i in range(len(link)):
            if link[i] < 0 or link[i] > 5:
                raise FK_Error

        for i in np.arange(link[0], link[1]+1):
            out = out.dot(dh2tf_1link(i))

    if grpTF is False:
        return T_w0.dot(out)
    else:
        return T_w0.dot(out.dot(T_6g))

def FK_pox(joint_angles):
    """
    TODO: implement this function

    Calculate forward kinematics for rexarm
    using product of exponential formulation

    return a 4-tuple (x, y, z, phi) representing the pose of the 
    desired link

    note: phi is the euler angle about y in the base frame

    """
    pass

def IK(pose):
    """
    Calculate inverse kinematics for rexarm
    Input - pose: the desired pose of the gripper in the form of homogeneous transformation matrix.
    Output - thetaset: a set of the required joint angles in numpy.array.
    """
    """
    Here are the steps:
        1. Calculate T_06 from pose.
            T_6g known (defined by gripper design), T_w0 known (world frame to frame 0), pose = T_wg (world to gripper)
            pose = T_wg = T_w0*T_06*T_6g => T_06 = inv(T_w0)*pose*inv(T_6g)

        2. IK: Use solvePieper() to calculate the Pieper's solution for the first 3 frames => theta 1, 2, 3. 
           Then use FK_dh() to check the solutions. If the pose is unreachable (outside of the workspace of the arm), 
           solvePieper() will come up with a solution that is the nearest to the desired position.

        3. Calculate the new P_04 with theta 1, 2, 3 if the desired pose is unreachable. Update the new pose.
        
        4. Use T^0_6 = T^0_3 * T^3_6 to obtain T^3_6

        5. IK: Use ZYZ Euler angle to solve theta 4, 5, 6 (Check function again!)
    """
    ## Part 1
    ## TODO: rewrite the definition of pose
    T_06 = inv4TF(T_w0).dot(pose.dot(inv4TF(T_6g))) ##################### TODO: be aware of the T_6g !!!!!!
    pose = pose.dot(inv4TF(T_6g))

    ## Part 2
    theta123 = solvePieper(T_06[0:4, 3])

    ## Part 3
    thetaSet = []
    for i, theta in enumerate(theta123):

        # Update new pose if pose is outside of workspace
        command = [theta[0], theta[1], theta[2], 0, 0, 0]
        P_04_new = FK_dh(command, 3)[:, 3]
        pose[:, 3] = P_04_new

    ## Part 4
        T_03 = inv4TF(T_w0).dot( FK_dh([theta[0], theta[1], theta[2], 0, 0, 0], 2) )
        T_x270 = np.array([1, 0, 0, 0, 0, 0, -1, 0, 0, 1, 0, 0, 0, 0, 0, 1]).reshape((4,4))
        T_zyz = inv4TF(T_03.dot(T_x270)).dot(T_06)

    ## Part 5
        thetaset456 = get_euler_angles_from_T(T_zyz)
        if len(thetaset456) == 2:
            for j, theta456 in enumerate(thetaset456):
                T_test = FK_dh([theta[0], theta[1], theta[2], theta456[0], theta456[1], theta456[2]]) ###########TODO: add inv4TF(T_6g)
                z2 = FK_dh([theta[0], theta[1], theta[2], theta456[0], theta456[1], theta456[2]], 2)[2, 3]
                err = sum(sum((T_test - pose)**2))
                if err < CLOSETOZERO and z2 > 25:
                    thetaSet.append([theta[0], theta[1], theta[2], theta456[0], theta456[1], theta456[2]])
        else:
            theta456 = thetaset456
            T_test = FK_dh([theta[0], theta[1], theta[2], theta456[0], theta456[1], theta456[2]]) ###########TODO: add inv4TF(T_6g)
            z2 = FK_dh([theta[0], theta[1], theta[2], theta456[0], theta456[1], theta456[2]], 2)[2, 3]
            err = sum(sum((T_test - pose)**2))
            if err < CLOSETOZERO and z2 > 25:
                thetaSet.append([theta[0], theta[1], theta[2], theta456[0], theta456[1], theta456[2]])

    return np.array(thetaSet)


def solvePieper(P_04):
    """
    Solves the Pieper's solution for the first 3 frames.\n
    Input: P04 - 4x1 array
    Output: A list of all possible theta sets in [theta1, theta2, theta3].
    """
    ###############################################################
    ### Solve theta3
    r = sum(P_04[i]**2 for i in range(len(P_04)-1))
    sol_flag = (r - d[3]**2 - a[2]**2) / (2*d[3]*a[2])
    try:
        if np.abs(sol_flag) > 1:
            #print "\ntheta3 error\nsol_flag = ", sol_flag, "\n"
            raise IK_Error
    except IK_Error:
        # Prints warning in terminal and set P to the neareast point
        P_04[0:3] = ((d[3]**2 + a[2]**2 + 2*d[3]*a[2]) / r)**0.5 * P_04[0:3]
        sol_flag = np.sign(sol_flag)*1.0

    sol = np.arcsin(sol_flag)
    theta3 = np.array([sol, np.sign(sol)*(180*D2R - np.abs(sol))])
    ##############################################################
    ## Solve theta2
    f1 = d[3]*np.sin(theta3) + a[2] #A 2x1 Array !!!
    f2 = -d[3]*np.cos(theta3)       #A 2x1 Array !!!
    s_a = -f2
    s_b = -f1
    s_c = P_04[2]
    theta2 = np.zeros([len(theta3), 2])
    for i in range(len(theta3)):
        sol_flag = s_b[i]**2 + s_a[i]**2 - s_c**2
        if sol_flag < 0 and np.abs(sol_flag) > CLOSETOZERO:
            # TODO: raise ERROR -> NO SOLUTION!
            print ("theta2 error!\nsol_flag = ", sol_flag)
        else:
            try:
                if sol_flag < CLOSETOZERO:
                    sol_flag = 0
                sol = np.array([(s_b[i] + sol_flag**0.5)/(s_a[i] + s_c), (s_b[i] - sol_flag**0.5)/(s_a[i] + s_c)])
                theta2[i, :] = 2*np.arctan(sol)

            except ZeroDivisionError:
                theta2[i, :] = np.array([180, 180])*D2R

    ###############################################################
    ## Solve theta1
    theta1 = np.zeros([len(theta3), len(theta2), 2])
    sol = 0
    for i in range(len(theta3)):
        for j in range(len(theta2)):
            sol_flag = P_04[0] / (f1[i]*np.cos(theta2[i, j]) - f2[i]*np.sin(theta2[i, j]))
            #print sol_flag
            if np.abs(sol_flag) > 1:
                if f1[i]*np.cos(theta2[i, j]) - f2[i]*np.sin(theta2[i, j]) < CLOSETOZERO:
                    sol_flag = 1
                elif np.abs(sol_flag) - 1 < CLOSETOZERO:
                    sol_flag = np.sign(sol_flag)*1.0
                else:
                    raise IK_Error
                    print ("theta1 error\n")
            sol = np.arccos(sol_flag)
            theta1[i, j, :] = np.array([sol, -sol])
    ###############################################################
    ## Theta offset
    theta1 = theta1 - theta_offset[0]
    theta2 = theta2 - theta_offset[1]
    theta3 = theta3 - theta_offset[2]

    ###############################################################
    ## Check for all answers
    thetalist = []
    err = np.zeros(len(theta3)*len(theta2)*len(theta1))
    for i in range(len(theta3)):
        for j in range(len(theta2)):
            for k in range(len(theta1)):
                T = inv4TF(T_w0).dot(FK_dh([theta1[i,j,k], theta2[i,j], theta3[i], 0, 0, 0], 2))
                P_test = T.dot(np.array([a[3], -d[3]*np.sin(alpha[3]), d[3]*np.cos(alpha[3]), 1]))
                err = sum((P_test - P_04)**2)
                if err < CLOSETOZERO:
                    thetalist.append([theta1[i,j,k], theta2[i,j], theta3[i]])
    return wrap(np.array(thetalist))

def get_euler_angles_from_T(T):
    """
    TODO: implement this function
    return the Euler angles from a T matrix
    
    """
    sol_flag = T[2,0]**2 + T[2,1]**2
    if sol_flag > CLOSETOZERO:
        thetalist = []
        t5 = np.arctan2((T[2,0]**2 + T[2,1]**2)**0.5, T[2,2])
        theta5 = [t5, -t5]
        theta4 = np.arctan2(T[1,2]/np.sin(theta5), T[0,2]/np.sin(theta5))
        theta6 = np.arctan2(T[2,1]/np.sin(theta5), -T[2,0]/np.sin(theta5))
        for i in range(2):
            thetalist.append([theta4[i], theta5[i], theta6[i]])
    else:
        if np.sign(T[2,2]) > 0:
            theta5 = 0
            theta4 = 0
            theta6 = np.arctan2(-T[0,1], T[0,0])
            thetalist = [theta4, theta5, theta6]
        else:
            theta5 = 180*D2R
            theta4 = 0
            theta6 = np.arctan2(T[0,1], -T[0,0])
            thetalist = [theta4, theta5, theta6]
    
    return wrap(np.array(thetalist))

def get_pose_from_T(T):
    """
    TODO: implement this function
    return the joint pose from a T matrix
    of the form (x,y,z,phi) where phi is rotation about base frame y-axis
    
    """
    pass

def to_s_matrix(w,v):
    """
    TODO: implement this function
    Find the [s] matrix for the POX method e^([s]*theta)
    """
    pass

def inv4TF(T):
    """
    Outputs the inverse of the transformation matrix not using the numpy.inv().\n
    Input: numpy.ndarray T
    """
    if not isinstance(T, np.ndarray):
        raise Inv_Error
    else:
        Tout = np.identity(4)
        R_T = np.transpose(T[0:3, 0:3])
        Tout[0:3, 0:3] = R_T
        Tout[0:3, 3] = -R_T.dot(T[0:3, 3])
        return Tout

def wrap(theta):
    """
    Transform any angle theta to -pi <= theta <= pi
    """    
    return np.mod((theta + np.pi), 2*np.pi) - np.pi

def blck2grp(blck_pose, pose = 'top'):
    """
    Transforms block pose to gripper pose.
    Input - blck_pose: should be a 4x1 np.array with [x, y, z, theta].
    Output - grp_pose: the gripper pose in the form of a homogeneous transformation matrix.
    """
    cos = np.cos
    sin = np.sin

    x = blck_pose[0]
    y = blck_pose[1]
    z = blck_pose[2]
    theta = blck_pose[3]

    T_wb = np.identity(4)
    T_wb[0,0] = cos(theta)
    T_wb[0,1] = -sin(theta)
    T_wb[0,3] = x
    T_wb[1,0] = sin(theta)
    T_wb[1,1] = cos(theta)
    T_wb[1,3] = y
    T_wb[2,3] = z
    
    T_gb = np.identity(4)
    T_wg = np.identity(4)
    if pose == 'top':   # grip from top
        T_gb[1,1] = -1
        T_gb[2,2] = -1
        T_gb[2,3] = 50
    if pose == 'side':
        pass

    T_wg = T_wb.dot(inv4TF(T_gb))
    return T_wg

def IK4blck(blck_pose, pose = 'top'):
    grp_pose = blck2grp(blck_pose, pose)
    #print grp_pose
    angleSets = IK(grp_pose)
    return angleSets



def test():
    """
    Test any functions here when executing kinematics.py on its own.
    """
    np.set_printoptions(formatter={'float': '{: 0.3f}'.format})
    
    #command = np.array([30, -90, 100, -40, 50, 60])*D2R
    #out1 = FK_dh(command, grpTF = True)# + np.array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 100, 0, 0, 0, 0]).reshape((4,4))
    #print out1
    #j_angle = IK(out1)
    start_point = [82.82589137010473, 122.96729378652523, -0.6396860693255348, 0]
    grp_pose = blck2grp(start_point)
    print ("\ngrp_pose = \n", grp_pose)
    j_angles = IK(grp_pose)
    print ("\nj_angles = \n", j_angles*R2D)
    #print IK4blck(start_point)*D2R

    """
    start_wrold = [82.82589137010473, 122.96729378652523, -0.6396860693255348, 0]
    pose = 
    [[  1.           0.           0.          82.82589137]
    [  0.          -1.           0.         122.96729379]
    [  0.           0.          -1.         189.36031393]
    [  0.           0.           0.           1.        ]]
    """

if __name__ == '__main__':
    test()