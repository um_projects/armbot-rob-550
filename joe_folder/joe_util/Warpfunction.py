from kinect import *
import numpy as np
import cv2

path_RGB = r'/home/student/Desktop/armlab-f19/joe_folder/RGB.png'
path_depth = r'/home/student/Desktop/armlab-f19/joe_folder/Depth.png'
depth2rgb_affine = np.float32([[1.00294423e+00,7.51337476e-05,-1.39662201e-01],[9.00474911e-05,1.00011838e+00,3.36899561e-01]])
        

img_RGB = cv2.imread(path_RGB) 
img_depth = cv2.imread(path_depth) 

img_warp_depth = cv2.warpAffine(img_depth, depth2rgb_affine, (640,480))
cv2.imshow('image', img_warp_depth) 

while True:
    ch = 0xFF & cv2.waitKey(10)
    if ch == 0x1B:
            break

cv2.imwrite('/home/student/Desktop/armlab-f19/joe_folder/Depth_Warped.png',img_warp_depth) 
cv2.destroyAllWindows()