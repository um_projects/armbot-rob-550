from kinect import *
import numpy as np
import cv2

img = cv2.cvtColor(freenect.sync_get_video()[0], cv2.COLOR_RGB2BGR)
hsv = cv2.cvtColor(img,cv2.COLOR_BGR2HSV)
kernel = np.ones((3, 3), np.uint8)
cv2.imshow('img',hsv)
while True:
    ch = 0xFF & cv2.waitKey(10)
    if ch == 0x1B:
            break
cv2.destroyAllWindows()
yellow_lower_range = np.array([26, 2, 233])
yellow_upper_range = np.array([38, 195, 254])
orange_lower_range = np.array([2, 171, 222])  
orange_upper_range = np.array([10, 236, 251])
pink_lower_range = np.array([165, 137, 247])
pink_upper_range = np.array([174, 171, 254])
black_lower_range = np.array([102, 26, 68])  
black_upper_range = np.array([123, 60, 83])
purple_lower_range = np.array([136, 76, 141])
purple_upper_range = np.array([149, 116, 163])
green_lower_range = np.array([58, 47, 125])
green_upper_range = np.array([72, 74, 143])
blue_lower_range = np.array([110, 150, 189])
blue_upper_range = np.array([114, 178, 208])
red_lower_range = np.array([172, 143, 155])
red_upper_range = np.array([178, 235, 182])

color_range_low = np.array([[26, 2, 233],[2, 171, 222],[165, 137, 247],[102, 26, 68],[136, 76, 141],[58, 47, 125],[110, 150, 189],[172, 143, 155]])
color_range_high = np.array([[38, 195, 254],[10, 236, 251],[174, 171, 254],[123, 60, 83],[149, 116, 163],[72, 74, 143],[114, 178, 208],[178, 235, 182]])
for i in range(8):
    mask = cv2.inRange(hsv,color_range_low[i],color_range_high[i])
    cv2.imshow('mask',mask)

    mask = cv2.morphologyEx(mask,cv2.MORPH_CLOSE,kernel)
    mask = cv2.morphologyEx(mask,cv2.MORPH_OPEN, kernel)

    _,contours,_ = cv2.findContours(mask,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
    res = cv2.bitwise_and(img,img,mask=mask)

    cv2.drawContours(freenect.sync_get_video()[0],contours,-1,(255,255,255),1)
    while True:
        ch = 0xFF & cv2.waitKey(10)
        if ch == 27:
            break

    cv2.destroyAllWindows()