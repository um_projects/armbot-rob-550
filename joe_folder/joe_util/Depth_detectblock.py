from kinect import *
import numpy as np
import cv2

D2R = 3.141592/180.0

depth_frame = freenect.sync_get_depth()[0]
np.clip(depth_frame,0,2**10 - 1,depth_frame)
depth_frame >>= 2
depth_frame = depth_frame.astype(np.uint8)
kernel = np.ones((3, 3), np.uint8)

#kernel = np.ones((3, 3), np.uint8)
cv2.imshow('img',depth_frame)
while True:
    ch = 0xFF & cv2.waitKey(10)
    if ch == 0x1B:
            break
cv2.destroyAllWindows()

yellow_lower_range = np.array([26, 2, 233])
yellow_upper_range = np.array([38, 195, 254])
orange_lower_range = np.array([2, 171, 222])  
orange_upper_range = np.array([10, 236, 251])
pink_lower_range = np.array([165, 137, 247])
pink_upper_range = np.array([174, 171, 254])
black_lower_range = np.array([102, 26, 68])  
black_upper_range = np.array([123, 60, 83])
purple_lower_range = np.array([136, 76, 141])
purple_upper_range = np.array([149, 116, 163])
green_lower_range = np.array([58, 47, 125])
green_upper_range = np.array([72, 74, 143])
blue_lower_range = np.array([110, 150, 189])
blue_upper_range = np.array([114, 178, 208])
red_lower_range = np.array([172, 143, 155])
red_upper_range = np.array([178, 235, 182])

color_range_low = np.array([[26, 2, 233],[2, 171, 222],[165, 137, 247],[102, 26, 68],[136, 76, 141],[58, 47, 125],[110, 150, 189],[172, 143, 155]])
color_range_high = np.array([[38, 195, 254],[10, 236, 251],[174, 171, 254],[123, 60, 83],[149, 116, 163],[72, 74, 143],[114, 178, 208],[178, 235, 182]])

block_range_low=np.array([173.5,169.5,164])
block_range_high=np.array([176.5,172.5,167.5])
block_area_range_low = np.array([550,550,550])
block_area_range_high = np.array([720,820,860])

cx_l = []
cy_l = []
cz_l = []
blockangle_l = []
area_l = []

for i in range(3):
    mask = cv2.inRange(depth_frame,block_range_low[i],block_range_high[i]) #175:one_block 171 two block 166 three block 
    cv2.imshow('mask',mask)

    mask = cv2.morphologyEx(mask,cv2.MORPH_CLOSE,kernel)
    mask = cv2.morphologyEx(mask,cv2.MORPH_OPEN, kernel)

    _,contours,_ = cv2.findContours(mask,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
    res = cv2.bitwise_and(depth_frame,depth_frame,mask=mask)

    cv2.drawContours(freenect.sync_get_video()[0],contours,-1,(255,255,255),1)
    while True:
        ch = 0xFF & cv2.waitKey(10)
        if ch == 27:
            break

    cv2.destroyAllWindows()

    for cnt in contours:
        cnt_area = cv2.contourArea(cnt)
        moments = cv2.moments(cnt)
        if moments['m00']!=0:
            cx = int(moments['m10']/moments['m00'])
            cy = int(moments['m01']/moments['m00'])
            cz = depth_frame[cy][cx]
            rect = cv2.minAreaRect(cnt)
            area = cv2.contourArea(cnt)
            print "area",area
            blockangle = abs(rect[2])*D2R
            box = cv2.boxPoints(rect)
            box = np.int0(box)
            im = cv2.drawContours(depth_frame,[box],0,(0,0,255),2) 
            if ((area > block_area_range_low[i]) & (area <block_area_range_high[i])):
                cx_l.append(cx)
                cy_l.append(cy)
                cz_l.append(cz)
                area_l.append(area)
                blockangle_l.append(blockangle)

    #cv2.drawContours(img,contours,-1,(255,255,255),2)
    #print cx_l,cy_l,cz_l,blockangle_l
print "kinect:",blockangle_l
print "x:",cx_l
print "y:",cy_l
print "z:",cz_l