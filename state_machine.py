import time
import numpy as np
import csv
from kinematics import IK4blck, IK, blck2grp
import copy


""" Radians to/from  Degrees conversions """
D2R = 3.141592/180.0
R2D = 180.0/3.141592

"""
TODO: Add states and state functions to this class
        to implement all of the required logic for the armlab
"""
class StateMachine():
    def __init__(self, rexarm, planner, kinect):
        self.rexarm = rexarm
        self.tp = planner
        self.kinect = kinect
        self.status_message = "State: Idle"
        self.current_state = "idle"
        self.next_state = "idle"
        ####################################################
        self.record_file = []
        self.pos_record = []
        self.speed_record = []
        self.image2WorldMatrix=[]

    def set_next_state(self, state):
        self.next_state = state

    """ This function is run continuously in a thread"""

    def run(self):
        if(self.current_state == "manual"):
            if (self.next_state == "manual"):
                self.manual()
            if(self.next_state == "idle"):
                self.idle()                
            if(self.next_state == "estop"):
                self.estop()
            if(self.next_state == "grptest"):
                self.grptest()
            if(self.next_state == "kinematicsCheck"):
                self.kinematicsCheck()
            if(self.next_state == "calibrate"):
                self.calibrate()

        if(self.current_state == "idle"):
            if(self.next_state == "manual"):
                self.manual()
            if(self.next_state == "idle"):
                self.idle()
            if(self.next_state == "estop"):
                self.estop()
            if(self.next_state == "calibrate"):
                self.calibrate()
            if(self.next_state == "record"):
                self.record()
            if(self.next_state == "execute"):
                self.execute()
            if(self.next_state == "play"):
                self.play()
            if(self.next_state == "detect"):
                self.detect()
            if(self.next_state == "picknstack"):
                self.picknstack()
            if(self.next_state == "linethemup"):
                self.linethemup()
            if(self.next_state == "stackhigh"):
                self.stackhigh()
            if(self.next_state == "grptest"):
                self.grptest()
            if(self.next_state == "kinematicsCheck"):
                self.kinematicsCheck()
            if(self.next_state == "clickGrab"):
                self.clickGrab()
            if(self.next_state == "aprilCalibrate"):
                self.aprilCalibrate()
            
        if(self.current_state == "estop"):
            self.next_state = "estop"
            self.estop()  
        
    #####################################################################
    # Task 1.2
        if(self.current_state == "execute"):
            if(self.next_state == "estop"):
                self.estop()
            if(self.next_state == "idle"):
                self.idle()
            if(self.next_state== "calibrate"):
                self.calibrate()


    # Task 2.1
        if(self.current_state == "record"):
            if(self.next_state == "estop"):
                self.estop()
            if(self.next_state == "stop_record"):
                self.stop_record()
            if(self.next_state == "record"):
                self.record()
            if(self.next_state == "idle"):
                self.idle()

        if(self.current_state == "stop_record"):
            if(self.next_state == "estop"):
                self.estop()
            if(self.next_state == "idle"):
                self.idle()

        if(self.current_state == "play"):
            if(self.next_state == "estop"):
                self.estop()
            if(self.next_state == "idle"):
                self.idle()

    ## Task 5.1

        if(self.current_state == "calibrate"):
            if(self.next_state == "estop"):
                self.estop()
            if(self.next_state == "idle"):
                self.idle()
            if(self.next_state== "calibrate"):
                self.calibrate()

        if(self.current_state == "detect"):
            if(self.next_state == "estop"):
                self.estop()
            if(self.next_state == "idle"):
                self.idle()


        if(self.current_state == "grptest"):
            if(self.next_state == "estop"):
                self.estop()
            if(self.next_state == "idle"):
                self.idle()
            if(self.next_state == "grptest"):
                self.grptest()

        if(self.current_state == "kinematicsCheck"):
            if(self.next_state == "estop"):
                self.estop()
            if(self.next_state == "idle"):
                self.idle()
            if(self.next_state == "kinematicsCheck"):
                self.kinematicsCheck()


        ## Task 6.1

        if(self.current_state == "clickGrab"):
            if(self.next_state == "estop"):
                self.estop()
            if(self.next_state == "idle"):
                self.idle()
            if(self.next_state == "clickGrab"):
                self.clickGrab()
            if(self.next_state== "calibrate"):
                self.calibrate()

        if(self.current_state == "aprilCalibrate"):
            if(self.next_state == "estop"):
                self.estop()
            if(self.next_state == "idle"):
                self.idle()
            if(self.next_state == "aprilCalibrate"):
                self.aprilCalibrate()

            
        ## Event 1
        if(self.current_state == "picknstack"):
            if(self.next_state == "estop"):
                self.estop()
            if(self.next_state == "idle"):
                self.idle()
            if(self.next_state == "picknstack"):
                self.picknstack()

        #Event 2
        if(self.current_state == "linethemup"):
            if(self.next_state == "estop"):
                self.estop()
            if(self.next_state == "idle"):
                self.idle()
            if(self.next_state == "linethemup"):
                self.linethemup()
        
        #Event 3
        if(self.current_state == "stackhigh"):
            if(self.next_state == "estop"):
                self.estop()
            if(self.next_state == "idle"):
                self.idle()
            if(self.next_state == "stackhigh"):
                self.stackhigh()


    #####################################################################

    """Functions run for each state"""


    def manual(self):
        self.status_message = "State: Manual - Use sliders to control arm"
        self.current_state = "manual"
        self.rexarm.send_commands()
        self.rexarm.get_feedback()

    def idle(self):
        self.status_message = "State: Idle - Waiting for input"
        self.current_state = "idle"
        self.rexarm.get_feedback()

    def estop(self):
        self.status_message = "EMERGENCY STOP - Check Rexarm and restart program"
        self.current_state = "estop"
        self.rexarm.disable_torque()
        self.rexarm.get_feedback()
       
    def calibrate(self):
        self.current_state = "calibrate"
        self.next_state = "idle"
        self.rexarm.set_speeds(3.0*np.ones(self.rexarm.num_joints))


        self.rexarm.set_positions(np.array([0, -80, 0, 0, 0, 0])*D2R)

        location_strings = ["lower left corner of board",
                            "upper left corner of board",
                            "upper right corner of board",
                            "lower right corner of board",
                            "center of shoulder motor"]
        i = 0
        for j in range(5):
            self.status_message = "Calibration - Click %s in RGB image" % location_strings[j]
            while (i <= j):
                self.rexarm.get_feedback()
                if self.kinect.new_click == True :
                    self.kinect.rgb_click_points[i] = self.kinect.last_click.copy()
                    i = i + 1
                    self.kinect.new_click = False        
        '''
        i = 0
        for j in range(5):
            self.status_message = "Calibration - Click %s in depth image" % location_strings[j]
            while (i <= j):
                self.rexarm.get_feedback()
                if(self.kinect.new_click == True):
                    self.kinect.depth_click_points[i] = self.kinect.last_click.copy()
                    i = i + 1
                    self.kinect.new_click = False
        '''
        #print(self.kinect.rgb_click_points)
        #print(self.kinect.depth_click_points)

        #print(self.kinect.getAffineTransform(self.kinect.depth_click_points,self.kinect.rgb_click_points))

        """TODO Perform camera calibration here"""
        # Adding real world points coordinates in mm
        # lowerleft, upper left, lower right, lower right
        actual_world=np.array([ [-310,-308,0],
                                [-310, 298,0],
                                [ 299, 298,0],
                                [ 299,-308,0],
                                [   0,   0,130]])
        #actual_world_z=np.array([0,0,0,0,130])

        frame = self.kinect.currentDepthFrame
        #print(frame)
        #frame_warped = self.kinect.registerDepthFrame(frame)
        
        image_points=np.array(self.kinect.rgb_click_points)

        '''
        ####Depth to RGB Calibration
        image_points_depth=np.array(self.kinect.depth_click_points)
        self.kinect.depthRGBCalibrate(image_points,image_points_depth)
        '''

        ##Image to World Calibration
        depth=[]
        for point in self.kinect.rgb_click_points:
            depth.append(frame[point[1]][point[0]])
        #[intrinsic, distort]=self.kinect.loadCameraCalibration()
        depth=np.array(depth)
        #print(image_points)
        image_points_all=np.c_[image_points, depth]
        #print(image_points_all)

        self.kinect.calibrate(image_points_all, actual_world)
        self.kinect.writeExtrinsicMatrix()
        #print(image2WorldMatrix)
        self.rexarm.set_positions(np.array([0, 0, 0, 0, 0, 0])*D2R)

        self.status_message = "Calibration - Completed Calibration"
        time.sleep(1)

    def aprilCalibrate(self):
        self.status_message = "April Calibration"
        self.current_state = "aprilCalibrate"
        self.next_state = "idle"
        self.rexarm.set_speeds(3.0*np.ones(self.rexarm.num_joints))
        self.rexarm.set_positions(np.array([0, -80, 80, 0, 0, 0])*D2R)
        
        try: 
            image_points, actual_world = self.kinect.aprilTagDetect()
            self.kinect.calibrate(image_points, actual_world)
            self.status_message = "Calibration - Completed AprilTag Calibration"
            
        except Exception as e: 
        
            print(e)
            print("Try again. All tags not detected")
            self.current_state = "idle"
        self.rexarm.set_positions(np.array([0, 0, 0, 0, 0, 0])*D2R)

    #####################################################################
    # Task 1.2, 
    # Now includes trajectory planning as well. -Anand

    def execute(self):
        waypts = [[ 0.0, 0.0, 0.0, 0.0, 0.0],
                  [ 1.0, 0.8, 1.0, 0.5, 1.0],
                  [-1.0,-0.8,-1.0,-0.5, -1.0],
                  [-1.0, 0.8, 1.0, 0.5, 1.0],
                  [1.0, -0.8,-1.0,-0.5, -1.0],
                  [ 0.0, 0.0, 0.0, 0.0, 0.0]]

        self.current_state = "execute"
        self.next_state = "idle"
        self.status_message = "State: Executing waypoints"
        for i in range(len(waypts)-1):
            #if(self.next_state == "estop"):
            #    self.estop()
            self.tp.set_initial_wp(waypts[i])
            self.tp.set_final_wp(waypts[i+1])
            self.tp.go(max_speed=1)
            self.rexarm.get_feedback()            
            self.rexarm.pause(1)

    #####################################################################
    # Task 2.1

    def record(self):
        self.current_state = "record"
        self.next_state = "idle"
        self.status_message = "State: Recording..."
        self.rexarm.disable_torque()
        self.save2csv()

        #if(self.next_state == "record"):
        #    self.rexarm.get_feedback()
        #    # TODO: save both positions and speeds
        #    self.pos_record.append(list(self.rexarm.get_positions()))
        #    self.speed_record.append(list(self.rexarm.get_speeds()))
        #    self.rexarm.pause(0.05)

    #def stop_record(self):
    #    print(self.pos_record)
    #    self.current_state = "stop_record"
    #    self.next_state = "idle"
    #    self.status_message = "State: Stop recording and saving files"
    #    self.rexarm.enable_torque()
    #    #self.rexarm.get_feedback()
    #    self.save2csv()
    #    print("=========================================Save Done=========================================")

    def save2csv(self):
        with open('Record.csv', 'a') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(np.array(self.rexarm.get_positions()))
        csvfile.close()

        #with open("Record.csv", 'w') as csvfile:
        #    writer = csv.writer(csvfile)
        #    for i, row in enumerate(self.pos_record):
        #        writer.writerow(row)
        #        #TODO: speed_record 
        #csvfile.close()

    def play(self):
        self.current_state = "play"
        self.next_state = "idle"
        self.status_message = "State: Playing"
        self.rexarm.enable_torque()
        with open('Record.csv', 'r') as wpts:
            reader = csv.reader(wpts,delimiter=',')
            reader=np.array(list(reader)).astype(float)
            for i in range(len(reader)):
            #if(self.next_state == "estop"):
            #    self.estop()
                self.rexarm.set_positions(reader[i])
                self.rexarm.get_feedback()            
                self.rexarm.pause(2)
        wpts.close()
            
    def detect(self):
        self.current_state = "detect"
        self.next_state = "idle"
        self.status_message = "State: detecting"
        self.kinect.blockDetector()

    def picknstack(self):
        self.current_state = "picknstack"
        self.next_state = "idle"
        self.status_message = "State: picknstack"
        [x,y,z,blockangle_l,color_l,score_l,index_l] = self.kinect.blockDetector()
        sorted_score_l = copy.deepcopy(score_l)
        sorted_score_l.sort(reverse = True)
        # click the stack point
        flag=False
        while(flag==False):
            self.rexarm.get_feedback()
            #print("waiting")
            if self.kinect.new_click == True:
                self.kinect.rgb_click_points = self.kinect.last_click.copy()
                self.kinect.new_click = False
                flag=True

        end_pos=self.kinect.rgb_click_points 
        
        #print ("sorted",score_l)
        offset = 45
        inner_offset = 0
        frame = self.kinect.currentDepthFrame
        depth = frame[end_pos[1]][end_pos[0]]
        [wx,wy,wz] = self.kinect.transformImage2World(end_pos[0],end_pos[1],depth)
        for score in sorted_score_l:    
            
            end_world = [wx,wy,wz+3+inner_offset,0]

            cur_index = score_l.index(score)
            [sx,sy,sz] = self.kinect.transformImage2World(x[cur_index],y[cur_index],z[cur_index])
            start_world = [sx,sy,sz,0]
            
            self.point_to_point(start_world,end_world)
            inner_offset = inner_offset + offset
            
        self.status_message = "Status: picknstack - Done."
        print("Done")
    
    def linethemup(self):
        visited_block = []
        self.current_state = "linethemup"
        self.next_state = "idle"
        self.status_message = "State: linethemup"
        [x,y,z,blockangle_l,color_l,score_l,_] = self.kinect.blockDetector()
        sorted_score_l = copy.deepcopy(score_l)
        sorted_score_l.sort(reverse = True)
        # click the stack point
        flag=False
        while(flag==False):
            self.rexarm.get_feedback()
            #print("waiting")
            if self.kinect.new_click == True:
                self.kinect.rgb_click_points = self.kinect.last_click.copy()
                self.kinect.new_click = False
                flag=True

        end_pos=self.kinect.rgb_click_points 
        
        #print ("sorted",score_l)
        offset = 59
        inner_offset = 0

        '''
        for score in sorted_score_l:    
            frame = self.kinect.currentDepthFrame 
            depth = frame[end_pos[1]][end_pos[0]]
            [wx,wy,wz] = self.kinect.transformImage2World(end_pos[0],end_pos[1],depth)
            end_world = [wx,wy-inner_offset,wz,0]

            cur_index = score_l.index(score)
            [sx,sy,sz] = self.kinect.transformImage2World(x[cur_index],y[cur_index],z[cur_index])
            start_world = [sx,sy,sz,0]
            
            self.point_to_point(start_world,end_world)
            inner_offset = inner_offset + offset
            '''
        while (len(sorted_score_l)!= 0):
            score = sorted_score_l.pop(0)
            visited_block.append(score)
            #print("visited_block",visited_block) 
            #print("sorted_score_l",sorted_score_l)
            frame = self.kinect.currentDepthFrame 
            depth = frame[end_pos[1]][end_pos[0]]
            [wx,wy,wz] = self.kinect.transformImage2World(end_pos[0],end_pos[1],depth)
            end_world = [wx,wy+inner_offset,wz,0]

            cur_index = score_l.index(score)
            [sx,sy,sz] = self.kinect.transformImage2World(x[cur_index],y[cur_index],z[cur_index])
            start_world = [sx,sy,sz,0]
            
            self.point_to_point(start_world,end_world)
            inner_offset = inner_offset + offset
            if z[cur_index]<=700.5:
                [x,y,z,blockangle_l,color_l,score_l,_] = self.kinect.blockDetector()
                score_temp = copy.deepcopy(score_l)
                for score_t in score_temp:
                    if (score_t in visited_block):
                        i = score_l.index(score_t)
                        score_l.pop(i)
                        x.pop(i)
                        y.pop(i)
                        z.pop(i)
                        blockangle_l.pop(i)
                        color_l.pop(i)
                
                sorted_score_l = copy.deepcopy(score_l)
                sorted_score_l.sort(reverse = True)
                print("sorted_score",sorted_score_l)


        self.status_message = "Status: linethemup - Done."
        print("Done")


    def stackhigh(self):
        visited_block = []
        self.current_state = "stackhigh"
        self.next_state = "idle"
        self.status_message = "State: stackhigh"
        [x,y,z,blockangle_l,color_l,score_l,_] = self.kinect.blockDetector()
        sorted_score_l = copy.deepcopy(score_l)
        sorted_score_l.sort(reverse = True)
        # click the stack point
        flag=False
        while(flag==False):
            self.rexarm.get_feedback()
            #print("waiting")
            if self.kinect.new_click == True:
                self.kinect.rgb_click_points = self.kinect.last_click.copy()
                self.kinect.new_click = False
                flag=True

        end_pos=self.kinect.rgb_click_points 
        
        #print ("sorted",score_l)
        offset = 50
        inner_offset = 0
        '''
        for score in sorted_score_l:    
            frame = self.kinect.currentDepthFrame 
            depth = frame[end_pos[1]][end_pos[0]]
            [wx,wy,wz] = self.kinect.transformImage2World(end_pos[0],end_pos[1],depth)
            end_world = [wx,wy,wz+inner_offset,0]

            cur_index = score_l.index(score)
            [sx,sy,sz] = self.kinect.transformImage2World(x[cur_index],y[cur_index],z[cur_index])
            start_world = [sx,sy,sz,0]
            
            self.point_to_point(start_world,end_world)
            inner_offset = inner_offset + offset
        '''
        
        while (len(sorted_score_l)!= 0):
            score = sorted_score_l.pop(0)
            visited_block.append(score)
            print("visited_block",visited_block) 
            print("sorted_score_l",sorted_score_l)
            frame = self.kinect.currentDepthFrame 
            depth = frame[end_pos[1]][end_pos[0]]
            [wx,wy,wz] = self.kinect.transformImage2World(end_pos[0],end_pos[1],depth)
            end_world = [wx,wy,wz+inner_offset,0]

            cur_index = score_l.index(score)
            [sx,sy,sz] = self.kinect.transformImage2World(x[cur_index],y[cur_index],z[cur_index])
            start_world = [sx,sy,sz,0]
            
            self.point_to_point(start_world,end_world)
            inner_offset = inner_offset + offset
            print("z[cur_index]",z[cur_index])
            if z[cur_index]<=700.5:
                [x,y,z,blockangle_l,color_l,score_l,_] = self.kinect.blockDetector()
                score_temp = copy.deepcopy(score_l)
                for score_t in score_temp:
                    if (score_t in visited_block):
                        i = score_l.index(score_t)
                        score_l.pop(i)
                        x.pop(i)
                        y.pop(i)
                        z.pop(i)
                        blockangle_l.pop(i)
                        color_l.pop(i)
                print("score_l",score_l)
                sorted_score_l = copy.deepcopy(score_l)
                sorted_score_l.sort(reverse = True)
                #print("sorted_score",sorted_score_l)
            
            
        self.status_message = "Status: picknstack - Done."
        print("Done")

    def grptest(self):
        self.current_state = "grptest"
        self.next_state = "idle"
        self.status_message = "State: Gripper testing..."
        self.rexarm.open_gripper()
        self.rexarm.pause(2)
        self.rexarm.close_gripper()
        self.rexarm.pause(1)

    def kinematicsCheck(self):
        self.current_state = "kinematicsCheck"
        self.next_state = "idle"
        self.status_message = "State: Checking FK and IKs..."
        import kinematics
        D2R = 3.141592/180.0
        pose = kinematics.FK_dh(np.array([11, 30, -78, 40, -74, 0])*D2R, grpTF = True)
        self.rexarm.set_wrist_pose(pose)
        #self.rexarm.set_positions(np.array([8.92111100e+01, -4.25236373e+01, -4.44837090e+01, -1.500000e+02, 9.29926911e+01,  8.92111110e+01])*D2R)

    def grab(self, world): 
        try:
            [x,y,z,_]=world
            self.rexarm.open_gripper()
            self.rexarm.pause(1)
            joint_start=self.rexarm.optimumIK([x,y,z,0])
            joint_end=self.rexarm.optimumIK([x,y,z-60,0])
            #joint_final=self.rexarm.optimumIK([x,y,z+60,0])
            '''
            print(z)
            print("JointStartGrab",joint_start*R2D)
            print("JointEndGrab",joint_end*R2D)
            self.tp.set_initial_wp(joint_start)
            self.tp.set_final_wp(joint_end)
            '''
            self.rexarm.set_positions(joint_end)
            self.rexarm.pause(1)

            '''
            self.tp.go(max_speed=0.5)
            self.rexarm.get_feedback()
            self.rexarm.pause(1)
            '''
            self.rexarm.close_gripper()
            self.rexarm.pause(1)
            '''
            self.rexarm.pause(1)
            self.tp.set_initial_wp(joint_end)
            self.tp.set_final_wp(joint_start)
            '''
            self.rexarm.set_positions(joint_start)
            self.rexarm.pause(1)
            '''
            self.tp.go(max_speed=2)
            self.rexarm.get_feedback()
            self.rexarm.pause(1)
            '''
        except:
            print("Error")

    def release(self, world): 
        try:
            [x,y,z,_]=world
            #self.rexarm.open_gripper()
            #self.rexarm.pause(1)
            joint_start=self.rexarm.optimumIK([x,y,z,0])
            joint_end=self.rexarm.optimumIK([x,y,z-35,0])
            #joint_final=self.rexarm.optimumIK([x,y,z+60,0])
            #print(z)
            print("JointStartRelease",joint_start*R2D)
            print("JointEndRelease",joint_end*R2D)
            self.tp.set_initial_wp(joint_start)
            self.tp.set_final_wp(joint_end)
            #self.rexarm.set_positions(joint_start)
            
            self.tp.go(max_speed=0.5)
            self.rexarm.get_feedback()
            self.rexarm.pause(1)

            self.rexarm.open_gripper()
            self.rexarm.pause(1)
            self.tp.set_initial_wp(joint_end)
            self.tp.set_final_wp(joint_start)
            #self.rexarm.set_positions(joint_start)

            self.tp.go(max_speed=2)
            self.rexarm.get_feedback()
            self.rexarm.pause(1)
        except:
            print("Release Error")


    def clickGrab(self):
        self.current_state = "clickGrab"
        self.next_state = "idle"
        self.status_message = "Click and Grab!! Click on a box in the image"
        
        flag=False
        while(flag==False):
            self.rexarm.get_feedback()
            #print("waiting")
            if self.kinect.new_click == True:
                self.kinect.rgb_click_points = self.kinect.last_click.copy()
                self.kinect.new_click = False
                flag=True

        start=self.kinect.rgb_click_points
        print ("start = ", start)
        #print("Start "+str(start))
        frame=self.kinect.currentDepthFrame 
        depth=frame[start[1]][start[0]]

        [x,y,z]=self.kinect.transformImage2World(start[0],start[1],depth)
        start_world=[x,y,z,0]
        #print "\nstart_world = ", start_world

        self.status_message = "Click and Grab!! Click on the destination point"
        flag=False
        while(flag==False):
            self.rexarm.get_feedback()
            #print("waiting")
            if self.kinect.new_click == True:
                self.kinect.rgb_click_points = self.kinect.last_click.copy()
                self.kinect.new_click = False
                flag = True
        end = self.kinect.rgb_click_points
        frame = self.kinect.currentDepthFrame
        depth = frame[end[1]][end[0]]

        [x,y,z]=self.kinect.transformImage2World(end[0],end[1],depth)
        end_world=[x,y,z+30,0]
        #print "end_world = ", end_world
        
        self.point_to_point(start_world,end_world)
        
        self.status_message = "Status: Click & Grab - Done."
        print("Done")

    def point_to_point(self, start_world, end_world):
        #[x2,y2,z2,_]=end_world
        #final_word = [x2,y2,z2,0]
        joint_start = self.rexarm.optimumIK(start_world)#self.rexarm.go2Block(start_world, np.array([0, 0, 0, 0, 0, 0]))
        joint_end = self.rexarm.optimumIK(end_world)#self.rexarm.go2Block(end_world)
        print("JointsFiltered ",joint_start*R2D)
        print("JointsFiltered ", joint_end)
        #joint_start=joint_start[0,:]
        #joint_end=joint_end[0,:]
        '''
        self.rexarm.set_wrist_pose(joint_start_pose)
        self.rexarm.pause(5)
        self.rexarm.set_wrist_pose(joint_end_pose)
        self.rexarm.pause(5)
        '''
        
        self.tp.set_initial_wp(np.zeros(self.rexarm.num_joints)) #np.array([0, 0, 0, 0, 0, 0])
        self.tp.set_final_wp(joint_start)
        self.tp.go(max_speed=2)
        #self.rexarm.set_positions(joint_start)
        self.rexarm.get_feedback()
        #self.rexarm.pause(5)
        #self.rexarm.set_positions(joint_end)
        self.rexarm.pause(1)

        print("start Grabbing")
        self.grab(start_world)
        print("end Grabbing")

        self.tp.set_initial_wp(joint_start)
        self.tp.set_final_wp(joint_end)
        #self.rexarm.set_positions(joint_start)
        self.tp.go(max_speed=2)
        self.rexarm.get_feedback()
        self.rexarm.pause(1)
        #self.rexarm.open_gripper()
        #self.rexarm.pause(2)

        print("start releasing")
        self.release(end_world)
        print("end releasing")

        self.tp.set_initial_wp(joint_end)
        self.tp.set_final_wp(np.zeros(self.rexarm.num_joints))
        self.tp.go(max_speed=2)
        self.rexarm.get_feedback()
        self.rexarm.pause(1)









        



