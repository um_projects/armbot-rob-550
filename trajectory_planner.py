import numpy as np 
import scipy.interpolate as interpolate
import time
import kinematics

"""
TODO: build a trajectory generator and waypoint planner 
        so it allows your state machine to iterate through
        the plan at the desired command update rate
"""
R2D = 180.0/3.141592
class TrajectoryPlanner():
    def __init__(self, rexarm):
        self.idle = True
        self.rexarm = rexarm
        self.num_joints = rexarm.num_joints
        self.initial_wp = [0.0]*self.num_joints
        self.final_wp = [0.0]*self.num_joints 
        self.dt = 0.1 # command rate default=0.05
        self.kinematics=kinematics
        self.stop_var=True
    
    def set_initial_wp(self, waypoint):
        self.initial_wp=waypoint
        print("Initial_wp",self.initial_wp*R2D)

    def set_final_wp(self, waypoint):
        self.final_wp=waypoint
        print("Final_wp",self.final_wp*R2D)

    def go(self, max_speed = 2.5):
        self.stop_var=False
        T = self.calc_time_from_waypoints(self.initial_wp,self.final_wp,max_speed)
        

        
        vel_plan=[]
        pos_plan=[]
        for i in range(self.num_joints):
            #set of coefficients for each joint's cubic spline
            a=self.generate_cubic_spline(self.initial_wp[i],self.final_wp[i],T)
            pos=[]
            vel=[]
            #calculate position and velocity commands for every dt
            for i in range(int(np.floor(T/self.dt))):
                t=i*self.dt
                #these store each position or velocity calculation in array pos and vel respectively, for given time and joint
                pos.append(a[0]    +   a[1]*t  +     (a[2]*(t**2)) +       (a[3]*(t**3)))
                vel.append(            a[1]    +       (2*a[2]*t)  +     (3*a[3]*(t**2)))    
            #Adds this joint's plan to the overall 2D array of joints
            vel_plan.append(vel)   
            pos_plan.append(pos)

        self.execute_plan(np.array(vel_plan),np.array(pos_plan))

    def stop(self):
        self.stop_var=True

    def calc_time_from_waypoints(self, initial_wp, final_wp, max_speed):
        
        # Need to check this -Anand
        diff=np.subtract(final_wp,initial_wp)
        largest=np.amax(np.absolute(diff))
        T=2*(largest/max_speed)

        return T

    def generate_cubic_spline(self, initial_wp, final_wp, T):
        #   Description of spline and coeffs
        #   q = a0 + a1*(t) + a2*(t^2) + a3*(t^3)
        #   a = [a0  a1  a2  a3] (column vector)
        
        t0=0
        tf=T
        q0=initial_wp
        qf=final_wp
        v0=0
        vf=0
        b = np.array([q0,v0,qf,vf])
        M = np.array([  [1,t0,t0**2,     t0**3],
                        [0, 1, 2*t0, 3*(t0**2)],
                        [1,tf,tf**2,     tf**3],
                        [0, 1, 2*tf, 3*(tf**2)] ])
        Minv = np.linalg.inv(M)
        a=np.matmul(Minv,b.transpose())

        return a

    def execute_plan(self, vel_plan, pos_plan, look_ahead=2):

        #j: time index. i: joint index
        #Plan is a 2D array. 
        #First index is to access a particular joint. 
        # Second index is to access the velocity at a given time.

        # Parameters for tuning. 
        # Look_ahead: Usually low numbers improve tracking performance to the spline.
        
        # pause = high numbers increase jerks... Very low numbers increase errors 

        #pause=0.02
        
        shape=vel_plan.shape
        for i in range(shape[1]):
            start=time.time()

            #To set historical speed data
            # if(i-look_ahead<0):
            #     self.rexarm.set_speeds_normalized_global(vel_plan[:,0])
            # else:
            #     self.rexarm.set_speeds_normalized_global(vel_plan[:,i-look_ahead])
            

            #To use future position data
            if(i+look_ahead>=shape[1]):
                self.rexarm.set_positions(self.final_wp)#pos_plan[:,-1])
                self.rexarm.set_speeds(0.5*np.ones(self.num_joints))
                
            else:
                self.rexarm.set_positions(pos_plan[:,i+look_ahead])
                self.rexarm.set_speeds(vel_plan[:,i])
            
            #print("Step "+ str(i))
            #waits till dt is complete
            while((time.time()-start)<self.dt):
                pass
            self.rexarm.get_feedback()
