from kinect import *
import numpy as np
import cv2


A=np.array([[5,2],[2,3],[2,2]])
B=np.array([[5,6],[2,3],[4,4]])

def getAffineTransform(coord1, coord2):
    """
    Given 2 sets of corresponding coordinates, 
    find the affine matrix transform between them.

    TODO: Rewrite this function to take in an arbitrary number of coordinates and 
    find the transform without using cv2 functions
    """

    pts1 = coord1[0:3].astype(np.float32)
    pts2 = coord2[0:3].astype(np.float32)
    #print pts1
    #print(cv2.getAffineTransform(pts1,pts2))
    #return cv2.getAffineTransform(pts1,pts2)
    len_c1= len(coord1);
    len_c2= len(coord2);

    #coord1=coord1.reshape(1,len_c1)
    #coord2=coord2.reshape(1,len_c2)
    #coord1_T=coord1.reshape(len_c1,1)
    coord1_T = coord1.transpose()   
    """
    A*M=B 
    M=(trans(A)A)^-1*trans(A)*B
    B is coord2, A is coord1
    """
    sq= np.matmul(coord1,coord1_T) #square matrix for AT*A
    inv_sq = np.linalg.pinv(sq) 
    M= np.matmul(coord2,np.matmul(coord1_T,inv_sq))
	
    print("M=",M)
    print("M2=",cv2.getAffineTransform(pts1,pts2))



getAffineTransform(A,B)