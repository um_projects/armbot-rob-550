#from kinect import transformImage2World
import numpy as np
import cv2

def detectall(depth_frame,img_frame, kinect):
    D2R = 3.141592/180.0
    img_frame_wCountour = cv2.cvtColor(img_frame,cv2.COLOR_BGR2RGB)
    depth_wCountour = depth_frame
    # depth_frame = freenect.sync_get_depth()[0]
    # np.clip(depth_frame,0,2**10 - 1,depth_frame)
    # depth_frame >>= 2
    # depth_frame = depth_frame.astype(np.uint8)
    # depth2rgb_affine = np.float32([[  9.33032351e-01, -3.28560156e-03, 6.92178173e+00],
    #                                             [  2.89633874e-03,  9.28260592e-01, 3.50516317e+01]])
    # depth_frame=cv2.warpAffine(depth_frame, depth2rgb_affine, (640,480))

    # img = cv2.cvtColor(freenect.sync_get_video()[0], cv2.COLOR_RGB2BGR)
    hsv = cv2.cvtColor(img_frame,cv2.COLOR_BGR2HSV)

    kernel = np.ones((3, 3), np.uint8)

    #kernel = np.ones((3, 3), np.uint8)
    # cv2.imshow('img',depth_frame)
    # while True:
    #     ch = 0xFF & cv2.waitKey(10)
    #     if ch == 0x1B:
    #             break
    # cv2.destroyAllWindows()



    #color_range_low = np.array([[26, 2, 233],[2, 171, 222],[165, 137, 247],[102, 26, 68],[136, 76, 141],[58, 47, 125],[110, 150, 189],[172, 143, 155]])
    #color_range_high = np.array([[38, 195, 254],[10, 236, 251],[174, 171, 254],[123, 60, 83],[149, 116, 163],[72, 74, 143],[114, 178, 208],[178, 235, 182]])

    block_range_low=np.array([700.5,682.5,662.5])
    block_range_high=np.array([708.5,694.5,674.5])
    block_area_range_low = np.array([400,500,500])
    block_area_range_high = np.array([610,670,722])

    cx_l = []
    cy_l = []
    cz_l = []
    blockangle_l = []
    area_l = []
    color_l = []
    score_l = []
    index_l = []

    for i in range(3):
        lowerb=block_range_low[i]
        higherb=block_range_high[i]
        mask = cv2.inRange(depth_frame,lowerb,higherb) 
        # cv2.imshow('mask',mask)

        mask = cv2.morphologyEx(mask,cv2.MORPH_CLOSE,kernel)
        mask = cv2.morphologyEx(mask,cv2.MORPH_OPEN, kernel)

        _,contours,_ = cv2.findContours(mask,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
        res = cv2.bitwise_and(depth_frame,depth_frame,mask=mask)

        #cv2.drawContours(freenect.sync_get_video()[0],contours,-1,(255,255,255),1)

        # while True:
        #     ch = 0xFF & cv2.waitKey(10)
        #     if ch == 27:
        #         break

        # cv2.destroyAllWindows()
        ind=0
        for cnt in contours:
            cnt_area = cv2.contourArea(cnt)
            moments = cv2.moments(cnt)
            if moments['m00']!=0:
                cx = int(moments['m10']/moments['m00'])
                cy = int(moments['m01']/moments['m00'])
                cz = depth_frame[cy][cx]
                rect = cv2.minAreaRect(cnt)

		        #box = cv2.boxPoints(rect)
		        #cv2.drawContours()

                area = cv2.contourArea(cnt)
                print ("area",area)
                temp_cl=[]

                blockangle = abs(rect[2])*D2R

                [wx,wy,wz]= kinect.transformImage2World(cx,cy,cz)
                if ((wx<0) & (wy>=0)):
                    blockangle = blockangle + np.pi / 2
                elif ((wx<=0) & (wy<0)):
                    blockangle = -blockangle - np.pi / 2
                elif ((wx>0) & (wy<0)):
                    blockangle = -blockangle 


                box = cv2.boxPoints(rect)
                box = np.int0(box)
                im = cv2.drawContours(depth_frame,[box],0,(0,0,255),2) 
                if ((area > block_area_range_low[i]) & (area <block_area_range_high[i])):       
                    temp_cl.append(checkcolor(cx,cy,hsv))
                    temp_cl.append(checkcolor(cx+5,cy,hsv))
                    temp_cl.append(checkcolor(cx+4,cy,hsv))
                    temp_cl.append(checkcolor(cx+3,cy,hsv))
                    temp_cl.append(checkcolor(cx+2,cy,hsv))
                    temp_cl.append(checkcolor(cx+1,cy,hsv))
                    temp_cl.append(checkcolor(cx-1,cy,hsv))
                    temp_cl.append(checkcolor(cx-2,cy,hsv))
                    temp_cl.append(checkcolor(cx-3,cy,hsv))
                    temp_cl.append(checkcolor(cx-4,cy,hsv))
                    temp_cl.append(checkcolor(cx-5,cy,hsv))
                    temp_cl.append(checkcolor(cx,cy+5,hsv))
                    temp_cl.append(checkcolor(cx,cy+4,hsv))
                    temp_cl.append(checkcolor(cx,cy+3,hsv))
                    temp_cl.append(checkcolor(cx,cy+2,hsv))
                    temp_cl.append(checkcolor(cx,cy+1,hsv))
                    temp_cl.append(checkcolor(cx,cy-5,hsv))
                    temp_cl.append(checkcolor(cx,cy-4,hsv))
                    temp_cl.append(checkcolor(cx,cy-3,hsv))
                    temp_cl.append(checkcolor(cx,cy-2,hsv))
                    temp_cl.append(checkcolor(cx,cy-1,hsv))
                    #print("temp_cl",temp_cl)
                    print("tempcolor",temp_cl)
                    color = most_frequent(temp_cl)
                    score = color_score(color)
                    cx_l.append(cx)
                    cy_l.append(cy)
                    cz_l.append(cz)
                    area_l.append(area)
                    blockangle_l.append(blockangle)
                    color_l.append(color)
                    score_l.append(score)
                    index_l.append(ind)
                    ind=ind+1
                    

                    if color == "yellow":
                        img_frame_wCountour=cv2.drawContours(depth_wCountour ,cnt,-1,(0,255,255),5)
                    elif color == "orange":
                        img_frame_wCountour=cv2.drawContours(depth_wCountour,cnt,-1,(0,128,255),5)
                    elif color == "green":
                        img_frame_wCountour=cv2.drawContours(depth_wCountour,cnt,-1,(0,255,0),5)
                    elif color == "pink":
                        img_frame_wCountour=cv2.drawContours(depth_wCountour,cnt,-1,(255,153,255),5)
                    elif color == "purple":
                        img_frame_wCountour=cv2.drawContours(depth_wCountour,cnt,-1,(153,0,153),5)
                    elif color == "blue":
                        img_frame_wCountour=cv2.drawContours(depth_wCountour,cnt,-1,(255,0,0),5)
                    elif color == "red":
                        img_frame_wCountour=cv2.drawContours(depth_wCountour,cnt,-1,(0,0,255),5)
                    elif color == "black":
                        img_frame_wCountour=cv2.drawContours(depth_wCountour,cnt,-1,(0,0,0),5)
                    
                    
        
        cv2.imwrite('/home/student/Desktop/armlab-f19/joe_folder/Depth_Warped.png',depth_wCountour) 
        #print cx_l,cy_l,cz_l,blockangle_l
    print ("blockangle_l:",blockangle_l)
    print ("x:",cx_l)
    print ("y:",cy_l)
    print ("z:",cz_l)
    print ("color:",color_l)
    print ("score:",score_l)
    print ("index:",index_l)
    return cx_l,cy_l,cz_l,blockangle_l,color_l,score_l,index_l

def checkcolor(cx,cy,hsv):
    hue = hsv[cy][cx][0]
    saturation = hsv[cy][cx][1]
    value = hsv[cy][cx][2]
    print("hue",hsv[cy][cx][0])
    print("satu",hsv[cy][cx][1])
    print("value",hsv[cy][cx][2])
    yellow_lower_range = np.array([82, 0, 50])
    yellow_upper_range = np.array([96, 255, 255])
    orange_lower_range = np.array([106, 50, 50])  
    orange_upper_range = np.array([119, 255, 255])
    pink_lower_range = np.array([129, 50, 50])
    pink_upper_range = np.array([137, 255, 255])
    black_lower_range = np.array([102, 26, 68])  
    black_upper_range = np.array([123, 60, 83])
    purple_lower_range = np.array([155, 10, 50])
    purple_upper_range = np.array([165, 255, 255])
    green_lower_range = np.array([38, 50, 50])
    green_upper_range = np.array([68, 255, 255])
    blue_lower_range = np.array([0, 50, 50])
    blue_upper_range = np.array([11, 255, 255])
    red_lower_range = np.array([121, 50, 50])
    red_upper_range = np.array([130, 255, 220])
    if ((hue > yellow_lower_range[0]) & (hue < yellow_upper_range[0]) & (saturation > yellow_lower_range[1]) & (saturation < yellow_upper_range[1]) & (value > yellow_lower_range[2]) & (value < yellow_upper_range[2])):
        color = "yellow"
        #score = 5
    elif ((hue > orange_lower_range[0]) & (hue < orange_upper_range[0]) & (saturation > orange_lower_range[1]) & (saturation < orange_upper_range[1]) & (value > orange_lower_range[2]) & (value < orange_upper_range[2])):
        color = "orange"
        #score = 6
    elif ((hue > pink_lower_range[0]) & (hue < pink_upper_range[0]) & (saturation > pink_lower_range[1]) & (saturation < pink_upper_range[1]) & (value > pink_lower_range[2]) & (value < pink_upper_range[2])):
        color = "pink"
        #score = 1
    elif ((hue > purple_lower_range[0]) & (hue < purple_upper_range[0]) & (saturation > purple_lower_range[1]) & (saturation < purple_upper_range[1]) & (value > purple_lower_range[2]) & (value < purple_upper_range[2])):
        color = "purple"
        #score = 2
    elif ((hue > green_lower_range[0]) & (hue < green_upper_range[0]) & (saturation > green_lower_range[1]) & (saturation < green_upper_range[1]) & (value > green_lower_range[2]) & (value < green_upper_range[2])):
        color = "green"
        #score = 4
    elif ((hue > blue_lower_range[0]) & (hue < blue_upper_range[0]) & (saturation > blue_lower_range[1]) & (saturation < blue_upper_range[1]) & (value > blue_lower_range[2]) & (value < blue_upper_range[2])):
        color = "blue"
        #score = 3
    elif ((hue > red_lower_range[0]) & (hue < red_upper_range[0]) & (saturation > red_lower_range[1]) & (saturation < red_upper_range[1]) & (value > red_lower_range[2]) & (value < red_upper_range[2])):
        color = "red"
        #score = 7
    else:
        color = "black"
        #score = 8
    return color

def most_frequent(List):
    return max(set(List), key=List.count)

def color_score(color):
    if color == "pink":
        score = 1
    elif color == "purple":
        score = 2
    elif color == "blue":
        score = 3
    elif color == "green":
        score = 4
    elif color == "yellow":
        score = 5
    elif color == "orange":
        score = 6
    elif color == "red":
        score = 7
    elif color == "black":
        score = 8
    return score
