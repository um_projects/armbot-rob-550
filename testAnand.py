
'''
import numpy as np
A=np.array([[1,0,2],[2,3,3]])
B=np.array([1,0,0])
C=np.array([1,1])
print(A)
print(np.c_[A,C])

open('last_extrinsic_cal.cfg', 'w')
'''
'''
import util.apriltag.python.apriltag as apriltag
import freenect
import cv2

window = 'Camera'
cv2.namedWindow(window)
detector = apriltag.Detector()

while True:

    frame = freenect.sync_get_video()[0]

    parser = ArgumentParser(
        description='test apriltag Python bindings')

    parser.add_argument('device_or_movie', metavar='INPUT', nargs='?', default=0,
                        help='Movie to load or integer ID of camera device')

    apriltag.add_arguments(parser)

    options = parser.parse_args()
    #rgb = cv2.cvtColor(frame, cv2.COLOR_RGB2BGR)
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    detections, dimg = detector.detect(frame, return_image=True)

    num_detections = len(detections)
    print('Detected {} tags.\n'.format(num_detections))

    for i, detection in enumerate(detections):
        print('Detection {} of {}:'.format(i+1, num_detections))
        print()
        print(detection.tostring(indent=2))
        print()

    overlay = frame // 2 + dimg[:, :] // 2

    cv2.imshow(window, overlay)
    k = cv2.waitKey(1)

    if k == 27:
        break
'''

import cv2
import numpy as np
from apriltag import apriltag
import freenect

while True:
    frame = freenect.sync_get_video()[0]
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)#cv2.imread(imagepath, cv2.IMREAD_GRAYSCALE)
    detector = apriltag("tagStandard41h12")

    detections = detector.detect(frame)
    if len(detections) is not 0:
        #print(detections[0]['lb-rb-rt-lt'])
        print(detections)
    else:
        print("No Detection")

    '''
    [[ 155.53227234,  448.25167847],
       [ 182.01390076,  446.44497681],
       [ 180.75056458,  419.65582275],
       [ 154.06628418,  421.88742065]])},)
    
[[ 144.96488953  439.87722778]
 [ 171.66217041  439.75515747]
 [ 172.10224915  413.18823242]
 [ 145.963974    412.90969849]]
    cv2.rectangle(frame, detections[0]['lb-rb-rt-lt'][0], detections[0]['lb-rb-rt-lt'][2])

    cv2.imshow(frame)
    k = cv2.waitKey(1)

    if k == 27:
        break
'''