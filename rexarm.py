#rexarm.py
import numpy as np
from kinematics import *
import time, sys

""" 
TODO:

Implement the missing functions
add anything you see fit

"""

""" Radians to/from  Degrees conversions """
D2R = 3.141592/180.0
R2D = 180.0/3.141592

class Rexarm():
    def __init__(self, joints, gripper):
        self.joints = joints
        self.gripper = gripper
        self.gripper_open_pos = np.deg2rad(50.0)
        self.gripper_closed_pos = np.deg2rad(-38.0)
        self.gripper_state = True
        self.estop = False
        
        """TODO: Find the physical angle limits of the Rexarm. Remember to keep track of this if you include more motors"""
        self.angle_limits = np.array([
                            [-180, 179.99],
                            [-122, 122],
                            [-115, 103],
                            [-150, 150],
                            [-130, 131],
                            [-150, 150]], dtype=np.float)*D2R
        self.grp_limits = np.array([-50, 50], dtype = np.float)*D2R
        """ Commanded Values """
        self.num_joints = len(joints)
        self.position = [0.0] * self.num_joints     # degrees
        self.speed = [1.0] * self.num_joints        # 0 to 1
        self.max_torque = [1.0] * self.num_joints   # 0 to 1
        self.grp_position = 0.0

        """ Feedback Values """
        self.joint_angles_fb = [0.0] * self.num_joints # degrees
        self.speed_fb = [0.0] * self.num_joints        # 0 to 1   
        self.load_fb = [0.0] * self.num_joints         # -1 to 1  
        self.temp_fb = [0.0] * self.num_joints         # Celsius
        self.move_fb = [0] *  self.num_joints

        #Last commanded Joint
        self.previousJoint=np.zeros(self.num_joints)

    def initialize(self):
        for joint in self.joints:
            joint.enable_torque()
            joint.set_position(0.0)
            joint.set_torque_limit(0.5)
            joint.set_speed(0.25)
        if(self.gripper != 0):
            self.gripper.set_torque_limit(1.0)
            self.gripper.set_speed(0.8)
            self.close_gripper()

    def grp_set_position(self, angle, update_now = True):
        angle = self.grp_clamp(angle)
        self.grp_position = angle
        if(update_now):
            self.gripper.set_position(angle)
    
    def grp_clamp(self, angle):
        if angle <= self.grp_limits[0]:
            angle = self.grp_limits[0]
            print ("Warning: Gripper angle exceeding minimum!")
        if angle >= self.grp_limits[1]:
            angle = self.grp_limits[1]
            print ("Warning: Gripper angle exceeding minimum!")
        return angle

    def open_gripper(self):
        """ TODO """
        self.gripper_state = False
        self.gripper.set_position(self.gripper_open_pos)

    def close_gripper(self):
        """ TODO """
        self.gripper_state = True
        self.gripper.set_position(self.gripper_closed_pos)

    def toggle_gripper(self):
        """ TODO """
        pass

    def set_positions(self, joint_angles, update_now = True):
        #joint_angles = self.clamp(joint_angles)
        for i,joint in enumerate(self.joints):
            self.position[i] = joint_angles[i]
            if(update_now):
                joint.set_position(joint_angles[i])

    
    def set_speeds_normalized_global(self, speed, update_now = True):
        for i,joint in enumerate(self.joints):
            self.speed[i] = speed
            if(update_now):
                joint.set_speed(speed)

    def set_speeds_normalized(self, speeds, update_now = True):
        for i,joint in enumerate(self.joints):
            self.speed[i] = speeds[i]
            if(update_now):
                joint.set_speed(speeds[i])

    def set_speeds(self, speeds, update_now = True):
        for i,joint in enumerate(self.joints):
            self.speed[i] = speeds[i]
            speed_msg = abs(speeds[i]/joint.max_speed)
            if (speed_msg < 3.0/1023.0):
                speed_msg = 3.0/1023.0
            if(update_now):
                joint.set_speed(speed_msg)
    
    def set_torque_limits(self, torques, update_now = True):
        for i,joint in enumerate(self.joints):
            self.max_torque[i] = torques[i]
            if(update_now):
                joint.set_torque_limit(torques[i])

    def send_commands(self):
        self.set_positions(self.position)
        self.set_speeds_normalized(self.speed)
        self.set_torque_limits(self.max_torque)
        if self.gripper == 0:
            pass
        else:
            self.grp_set_position(self.grp_position)

    def enable_torque(self):
        for joint in self.joints:
            joint.enable_torque()

    def disable_torque(self):
        for joint in self.joints:
            joint.disable_torque()

    def get_positions(self):
        for i,joint in enumerate(self.joints):
            self.joint_angles_fb[i] = joint.get_position()
        return self.joint_angles_fb

    def get_speeds(self):
        for i,joint in enumerate(self.joints):
            self.speed_fb[i] = joint.get_speed()
        return self.speed_fb

    def get_loads(self):
        for i,joint in enumerate(self.joints):
            self.load_fb[i] = joint.get_load()
        return self.load_fb

    def get_temps(self):
        for i,joint in enumerate(self.joints):
            self.temp_fb[i] = joint.get_temp()
        return self.temp_fb

    def get_moving_status(self):
        for i,joint in enumerate(self.joints):
            self.move_fb[i] = joint.is_moving()
        return self.move_fb

    def get_feedback(self):
        self.get_positions()
        self.get_speeds()
        self.get_loads()
        self.get_temps()
        self.get_moving_status()

    def pause(self, secs):
        time_start = time.time()
        while((time.time()-time_start) < secs):
            self.get_feedback()
            time.sleep(0.05)
            if(self.estop == True):
                break

    def clamp(self, joint_angles):
        #print("joint_angles",joint_angles)
        new_joint_angles = []
        for joint_angle in joint_angles:
            print("joint_angle",joint_angle)
            #exceed=False
            for i,joint in enumerate(joint_angle):
                if joint <= self.angle_limits[i][0]: 
                    joint = self.angle_limits[i][0]
                    print("Warning: theta[", i+1, "] excceding minimum")
                    #exceed=True
                elif joint >= self.angle_limits[i][1]:
                    joint = self.angle_limits[i][1]
                    print("Warning: theta[", i+1, "] exceeding maximum!")
                    #exceed=True

            #if exceed==False:
                #new_joint_angles.append(joint_angle)
        #print (np.array(new_joint_angles))
        #return np.array(new_joint_angles)
            
    def removeInvalidSols(self, joint_angles):
        print("joint_angles",joint_angles)
        new_joint_angles = []
        for joint_angle in joint_angles:
            print("joint_angle",joint_angle*R2D)
            exceed=False
            for i,joint in enumerate(joint_angle):
                if joint <= self.angle_limits[i][0]: 
                    #joint = self.angle_limits[i][0]
                    print("Warning: theta[", i+1, "] excceding minimum")
                    exceed=True
                elif joint >= self.angle_limits[i][1]:
                    #joint = self.angle_limits[i][1]
                    print("Warning: theta[", i+1, "] exceeding maximum!")
                    exceed=True

            if exceed==False:
                new_joint_angles.append(joint_angle)
        #print (np.array(new_joint_angles))
        if(len(new_joint_angles)==0):
            print("NO SOLUTIONS AVAILABLE!!!!!!!!!!")
        return np.array(new_joint_angles)

    def get_wrist_pose(self):
        """
        Outputs the current pose of frame wrist 6.
        """
        if len(self.joints) > 5:
            T = FK_dh(self.joint_angles_fb)
        else:
            T = FK_dh([self.joint_angles_fb[0], self.joint_angles_fb[1], self.joint_angles_fb[2], self.joint_angles_fb[3], self.joint_angles_fb[4], 0])
        x = T[0,3]
        y = T[1,3]
        z = T[2,3]
        zyzangles = get_euler_angles_from_T(T)*R2D
        if len(zyzangles) == 2:
            return [x, y, z, zyzangles[0,0], zyzangles[0,1], zyzangles[0,2]]
        else:
            return [x, y, z, zyzangles[0], zyzangles[1], zyzangles[2]]

    def set_wrist_pose(self, pose):
        """
        Calculates IK(pose) and outputs all joint angles.
        Currently, pose must be a homogeneous transformation matrix in SE3.
        """
        #print("IK(pose)",IK(pose))
        joint_angles = self.clamp(IK(pose))
        joint_angle = joint_angles[0,:]
        #print("joint_angle ",joint_angle)
        #joint_angles = IK(pose)[0,:]
        #print (joint_angles/D2R)
        self.set_positions(joint_angle)
        self.get_feedback()
        self.pause(2)

    
    def go2Block(self, blck_pose, past_angles = []):
        joint_angles = self.choose_j_angles(IK4blck(blck_pose), past_angles)
        #trajectory_planning()
        #self.set_positions(joint_angles)
        #self.get_feedback()
        return joint_angles



    def choose_j_angles(self, IKsolutions, past_angles = []):       ##TODO
        """
        Selects the proper joint angles from the IK() solution sets.
        """
        angleList = []
        # check if it's 2d array
        if len(np.shape(IKsolutions)) > 1:
            for i, angleset in enumerate(IKsolutions):
                flag = 0
                for j, joint_angle in enumerate(angleset):
                    if joint_angle < self.angle_limits[j,0] or joint_angle > self.angle_limits[j,1]:
                        flag = 1
                if np.sign(angleset[1]*angleset[2]) < 0:
                    flag = 1
                if flag == 0:
                    angleList.append(list(angleset))

            angleList = np.array(angleList)
            
            if len(np.shape(angleList)) != 2:
                # 1D array
                return angleList
            elif len(past_angles) == 0:
                return angleList[0,:]
            
            if len(past_angles) != 0 and len(np.shape(angleList)) > 1:
                tempList = []
                for i, angles in enumerate(angleList):
                    tempList.append([sum((angles - past_angles)**2), angles])
                tempList.sort()
                sol = tempList[0][1]
                return sol

    def angleOptimize(self,joint_angles_Set):
        angleDiffMin=10000
        solution=[]
        for joint_angles in joint_angles_Set:
            angleDiff=np.absolute(joint_angles[0]-self.previousJoint[0])
            if(angleDiff<angleDiffMin):
                solution=joint_angles
                angleDiffMin=angleDiff
        return solution
            

    def optimumIK(self, xyzTheta):
        #[x,y,z,theta]=xyzTheta
        joint_angles_Valid=self.removeInvalidSols(IK(blck2grp(xyzTheta)))
        joint_angle_Optimum=self.angleOptimize(joint_angles_Valid)
        self.previousJoint=joint_angle_Optimum

        return joint_angle_Optimum




